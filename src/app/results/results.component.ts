import { Component, Input, OnInit, AfterViewInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  @Input() data:any = [];
  longitude:any = Number;
  latitude:any = Number;

  constructor() { }

  ngOnInit(): void {
  }

  // Envoi des coordonnées sur la vue
 sendCoord(long: number, lat: number){
  console.log("long :" + long + " lat : " + lat);
  this.longitude = long;
  this.latitude = lat;
}

}
