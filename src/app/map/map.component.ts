import { Component, AfterViewInit, Input, SimpleChanges } from '@angular/core';
import * as L from 'leaflet';
import { icon, Marker } from 'leaflet';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
  private map: any;
  // Récuperation des coordonnées du parentComponent
  @Input() longitude:any= Number;
  @Input() latitude:any= Number;
  theMarker:any = {};


// Initialisation du map
  private initMap(): void {
    this.map = L.map('map', {
      center: [ 43.921098, 2.138793],
      zoom: 9
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  constructor() { }

  ngAfterViewInit(): void {
    this.initMap();
  }


  // Mise en place du marker sur le plan
  placeMarker() {

const iconUrl = 'assets/marker-icon-perso.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = icon({
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
Marker.prototype.options.icon = iconDefault;

    const location = [this.latitude, this.longitude];

    if (this.map != undefined){
      this.map.panTo(location);
      if (this.theMarker != undefined){
        this.map.removeLayer(this.theMarker);
      }
      this.theMarker = L.marker([this.latitude, this.longitude]).addTo(this.map);
    }
  }

  // Evenement pour mettre le marker à chaque changement de longitude ou latitude
  ngOnChanges(changes: SimpleChanges) {
    this.placeMarker();
  }
}
