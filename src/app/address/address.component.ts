import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})


export class AddressComponent implements OnInit {
  title = 'adresse-angular';
  data:any = [];
  address: any = String;


  constructor(private http: HttpClient) {}

//  récuperation des coordonnées sur l'api
 getAddress(address: any){
  this.address = address.replace(/[^a-zA-Z0-9]/g, "+");
  const url ='https://api-adresse.data.gouv.fr/search/?q='+ this.address;
  this.http.get(url).subscribe((res)=>{
    this.data = res;
  })
  console.log(this.address);

 }

 ngOnInit(): void {
}


}
